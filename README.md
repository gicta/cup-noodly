# cup-noodly

## Getting started
Giving [this article](https://programmingsoup.com/nodejs-api-project-architecture) a go for just the Docker piece.\
\
Later, I'm going to want to hit on some of the other topics, but through Git and likely pushing stuff to AWS.  I like some of the TDD/mock topics in the article as well, but again, not currently my focus.\
\
Mods:
- Used Fastify, not Express
- To get the host able to use http://localhost:3000 and reach the Docker container, the server.js must use
```
const fastify = require('fastify')
const app = fastify()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen({port: this.port, host: '0.0.0.0'}, () => {
  console.log(`Listening at http://localhost:${port}`)
})
```
OK, it works. Fastify|NodeJS is runninng on Docker and responding to requests from a host browser. 
That's it for now.  Next, for another day is using GitLab CICD to pump this to AWS.

